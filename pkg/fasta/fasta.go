package fasta

import (
	"fmt"
	"io/ioutil"
	"strconv"

	"bitbucket.org/JacobFerrier/permissive_hmm_baiting/pkg/common"
)

// Sequence object
type Sequence struct {
	sequence  string
	length    int
	id        string
	gcContent float32
}

// Fasta object
type Fasta struct {
	sequences    []Sequence
	numSequences int
	fileName     string
}

// NewSequence builds a new sequence object given the id and sequence length'
func NewSequence(sequenceLength int, id string) Sequence {
	sequence := common.GenerateRandomString(sequenceLength, "atcg")

	gcCount := 0
	for _, n := range sequence {
		if string(n) == "c" || string(n) == "g" {
			gcCount++
		}
	}

	return Sequence{sequence, sequenceLength, id, float32(gcCount) / float32(sequenceLength)}
}

// NewFasta builds a new Fasta object given the number of sequences and sequence length
func NewFasta(numSequences int, sequenceLength int, fileName string) Fasta {
	sequences := make([]Sequence, numSequences)
	for i := 0; i < numSequences; i++ {
		sequences[i] = NewSequence(sequenceLength, "seq"+strconv.Itoa(i))
	}

	return Fasta{sequences, numSequences, fileName}
}

//WriteToFile writes out a Fasta object to its file specified by fileName
func (f *Fasta) WriteToFile() {
	out := ""
	for i := 0; i < f.numSequences; i++ {
		s := f.sequences[i]
		out += ">" + s.id + "|len=" + strconv.Itoa(s.length) + "|gcContent=" + fmt.Sprintf("%g", s.gcContent) + "\n" + s.sequence + "\n"
	}

	err := ioutil.WriteFile(common.GetMainDir()+"/data/"+f.fileName, []byte(out), 0644)
	common.Check(err)
}
