package create

import (
	"bitbucket.org/JacobFerrier/permissive_hmm_baiting/pkg/common"
	"bitbucket.org/JacobFerrier/permissive_hmm_baiting/pkg/fasta"
)

// TestFasta creates and writes a test fasta file
func TestFasta(fileName string, numSequences int, sequenceLength int, writeOver bool) {
	fasta := fasta.NewFasta(numSequences, sequenceLength, fileName)

	if !common.FileCheck(common.GetMainDir()+"/data/"+fileName) || writeOver {
		fasta.WriteToFile()
	}
}
