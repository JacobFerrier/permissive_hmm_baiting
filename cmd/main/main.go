package main

import (
	"fmt"

	"bitbucket.org/JacobFerrier/permissive_hmm_baiting/pkg/create"
)

func main() {
	fmt.Print("\n-----\nCreating test fasta file\n-----\n\n")
	create.TestFasta("testFasta.fasta", 5, 50, true)
}
